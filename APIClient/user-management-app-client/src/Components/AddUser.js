import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import { Button, withStyles } from '@material-ui/core';

const styles = theme => ({
    table: {
        minWidth: 650,
    },
    root: {
        "& .MuiTextField-root": {
            margin: theme.spacing(1),
            width: "25ch"
        }
    }
});

class AddUser extends Component {
    state = {
        userName: "",
        userSurname: "",
        userPhoneNumbers: []
    };

    OnSubmitUser = async (event) => {
        event.preventDefault();
        const user = {
            Name: this.state.userName,
            Surname: this.state.userSurname,
            PhoneNumbers: this.state.userPhoneNumbers.map((phoneNumber) =>
                ({ PhoneNumber: phoneNumber }))
        }
        const resp = await axios.post(`https://localhost:44394/api/Users`, user);
        this.props.onUpdate();
        this.props.onCancel();
    }

    OnCancelClick = (event) => {
        this.props.onCancel();
    }

    OnAddPhoneNumberEntry = (event) => {
        this.setState({ userPhoneNumbers: [...this.state.userPhoneNumbers, ''] });
    }

    render() {
        const { classes } = this.props;
        return (
            <form onSubmit={this.OnSubmitUser}>
                <div className={classes.root}>
                    <TextField
                        required
                        id="filled-required"
                        label="Imię"
                        value={this.state.userName}
                        onChange={event => this.setState({ userName: event.target.value })}
                    />
                    <TextField
                        required
                        id="filled-required"
                        label="Nazwisko"
                        value={this.state.userSurname}
                        onChange={event => this.setState({ userSurname: event.target.value })}
                    />
                    {this.state.userPhoneNumbers.map((userPhoneNumber, index) => (
                        <TextField
                            key={index}
                            required
                            id="filled-required"
                            label="Telefon"
                            value={this.state.userPhoneNumber}
                            onChange={event => this.setState({
                                userPhoneNumbers: [
                                    ...this.state.userPhoneNumbers.slice(0, index),
                                    event.target.value,
                                    ...this.state.userPhoneNumbers.slice(index + 1)
                                ]
                            })}
                        />
                    ))}
                    <Button onClick={this.OnAddPhoneNumberEntry}>Dodaj numer</Button>
                    <div>
                        <Button type="submit" label="login" className="button-submit" primary={true}>Wyślij</Button>
                        <Button onClick={this.OnCancelClick}>Anuluj</Button>
                    </div>
                </div>
            </form>
        );
    }
}

export default withStyles(styles)(AddUser);