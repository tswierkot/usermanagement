import React, { Component } from 'react'
import UserList from './UserList'
import axios from 'axios';
import AddUser from './AddUser';
import UserDetails from './UserDetails';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { Button } from '@material-ui/core';

export default class Layout extends Component {
    state = {
        profiles: [],
        showing: false
    };

    componentDidMount = async () => {
        this.update();
    }

    update = async () => {
        const resp = await axios.get(`https://localhost:44394/odata/Users?$expand=PhoneNumbers`);
        this.setState({ profiles: resp.data });
    }

    render() {
        return (
            <>
                <Router>
                    <Switch>
                        <Route path="/UserDetails/:id" exact component={UserDetails}>
                        </Route>
                        <Route path="/">
                            <Button onClick={() => this.setState({ showing: !this.state.showing })}>Dodaj użytkownika</Button>
                            {this.state.showing
                                ? <AddUser onCancel={() => this.setState({ showing: false })} onUpdate={this.update} />
                                : null
                            }
                            <UserList profiles={this.state.profiles} />
                        </Route>
                    </Switch>
                </Router>
            </>
        )
    }
}
