import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
});

export default function UserList(props) {
	const classes = useStyles();
	const history = useHistory();

	function handleClick(userId) {
		history.push(`/UserDetails/${userId}`);
	}

	return (
		<TableContainer component={Paper}>
			<Table className={classes.table} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Imię</TableCell>
						<TableCell align="right">Nazwisko</TableCell>
						<TableCell align="right">Liczba numerów telefonu</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{props.profiles.map((user) => (
						<TableRow key={user.Id} onClick={() => handleClick(user.Id)}>
							<TableCell component="th" scope="row">
								{user.Name}
							</TableCell>
							<TableCell align="right">{user.Surname}</TableCell>
							<TableCell align="right">{user.PhoneNumbers.length}</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
}
