import React, { Component } from 'react'
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';

export default class UserDetails extends Component {
    state = {
        userData: null,
        userPhoneNumbers: [],
    };

    componentDidMount = async () => {
        this.getUserData();
    }

    getUserData = async () => {
        const { id } = this.props.match.params;
        const resp = await Axios.get(`https://localhost:44394/api/Users/${id}`);
        this.setState({ userData: resp.data });
    }

    addPhoneNumber = async () => {
        const phone = {
            PhoneNumber: this.state.userPhoneNumber,
            UserId: this.state.userData.id
        };

        const resp = await Axios.post(`https://localhost:44394/api/Phones/`, phone);
        this.setState({ userPhoneNumber: "" });
        this.getUserData();
    }

    render() {
        return (
            <>
                {
                    this.state.userData &&
                    <>
                        Imię:<strong> {this.state.userData.name}</strong>
                        <br />
                        Nazwisko: <strong> {this.state.userData.surname}</strong>
                        <br />
                        Numery telefonów:
                        <br />
                        {this.state.userData.phoneNumbers?.map((phone) => {
                            return <div>
                                - <strong> {phone.phoneNumber}</strong>
                                <br />
                            </div>
                        })}
                        <div>
                            <TextField
                                required
                                id="filled-required"
                                label="Telefon"
                                value={this.state.userPhoneNumber}
                                onChange={event => this.setState({ userPhoneNumber: event.target.value })}
                            />
                            <br />
                            <Button onClick={this.addPhoneNumber}>Dodaj</Button>
                        </div>
                    </>
                }
            </>
        )
    }
}
