﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Persistance.Entities;

namespace Persistance.DbContext
{
    public class UserManagementDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public UserManagementDbContext(DbContextOptions<UserManagementDbContext> options)
            : base(options)
        {

        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Phone>()
                .HasOne(phone => phone.User)
                .WithMany(user => user.PhoneNumbers);
        }
    }
}
