﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Persistance.Entities
{
    public class Phone
    {
        public Guid Id { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual User User { get; set; }
    }
}
