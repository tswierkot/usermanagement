﻿using Infrastructure.Mappers;
using Microsoft.EntityFrameworkCore;
using Models.DTO;
using Persistance.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class UserService : IUserService
    {
        private UserManagementDbContext _userManagementDbContext;

        public UserService(UserManagementDbContext userManagementDbContext)
        {
            _userManagementDbContext = userManagementDbContext;

        }

        public async Task<List<UserDto>> GetAllUsersWithPhones()
        {
            var users = await _userManagementDbContext
                .Users
                .Include(user => user.PhoneNumbers)
                .AsNoTracking()
                .ToListAsync();

            return users.ConvertToDTO().ToList();
        }

        public async Task<UserDto> GetUser(Guid id)
        {
            var user = await _userManagementDbContext
                .Users
                .Include(user => user.PhoneNumbers)
                .AsNoTracking()
                .FirstOrDefaultAsync(user => user.Id == id);

            return user.ConvertToDTO();
        }

        public async Task<UserDto> CreateUser(UserDto userDto)
        {
            var user = _userManagementDbContext.Users.Add(userDto.ConvertToEntity());
            await _userManagementDbContext.SaveChangesAsync();
            return user?.Entity?.ConvertToDTO();
        }
    }
}
