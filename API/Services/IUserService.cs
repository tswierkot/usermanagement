﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public interface IUserService
    {
        Task<List<UserDto>> GetAllUsersWithPhones();
        Task<UserDto> GetUser(Guid id);
        Task<UserDto> CreateUser(UserDto userDto);
    }
}
