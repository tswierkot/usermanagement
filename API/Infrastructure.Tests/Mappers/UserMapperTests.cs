﻿using Infrastructure.Mappers;
using Persistance.Entities;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Infrastructure.Tests.Mappers
{
    public class UserMapperTests
    {
        [Theory]
        [InlineData("c3303626-426b-4259-a790-a0bf5dd34f36", "UserName", "UserSurname", "")]
        [InlineData("ed762aa6-69a4-4435-b3ee-d804c8c8ca13", "UserName", "UserSurname", "12374854", "654654645")]
        [InlineData("572192ac-1291-4011-b6cc-9d1763a50f00", "UserName", "UserSurname", "12374854", "234225", "72342234225")]
        [InlineData("ab78d12a-ac92-42a2-9401-e28edf101014", "UserName", "UserSurname", "number123", "+48-32442-243")]
        public void ConvertToDTO_UserEntityObjectToMap_DtoShouldBeReturnedCorrectly(string id, string name, string surname, params string[] phones)
        {
            // Arrange 
            var userId = Guid.Parse(id);
            var phonesEntity = new List<Phone>();
            foreach (var phoneNumber in phones)
            {
                phonesEntity.Add(new Phone { PhoneNumber = phoneNumber, UserId = userId });
            }
            var userEntity = new User { Id = userId, Name = name, Surname = surname, PhoneNumbers = phonesEntity };

            // Act
            var userDto = userEntity.ConvertToDTO();

            // Assert
            userDto.Id.ShouldBe(userEntity.Id);
            userDto.Name.ShouldBe(userEntity.Name);
            userDto.Surname.ShouldBe(userEntity.Surname);
            userDto.PhoneNumbers.Count().ShouldBe(phones.Length);

            for (int i = 0; i < userDto.PhoneNumbers.Count(); i++)
            {
                var userDtoPhoneElement = userDto.PhoneNumbers.ElementAt(i);
                var userEntityPhoneElement = userEntity.PhoneNumbers.ElementAt(i);

                userDtoPhoneElement.PhoneNumber.ShouldBe(userEntityPhoneElement.PhoneNumber);
                userDtoPhoneElement.UserId.ShouldBe(userEntityPhoneElement.UserId);
            }
        }
    }
}
