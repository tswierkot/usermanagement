using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Persistance.DbContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.OData.Edm;
using Microsoft.AspNet.OData.Builder;
using Persistance.Entities;
using Services;
using Microsoft.OpenApi.Models;
using System.Linq;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNet.OData.Formatter;
using UserManagementApp.Validation;

namespace UserManagementApp
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly string DevelopmentPolicyCorsPolicy = "DevelopmentPolicyCorsPolicy";
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (_hostingEnvironment.IsDevelopment())
            {
                services.AddCors(options =>
                {
                    options.AddPolicy(name: DevelopmentPolicyCorsPolicy,
                        builder =>
                        {
                            builder
                                .AllowAnyHeader()
                                .AllowAnyOrigin()
                                .AllowAnyMethod()
                                .WithOrigins("https://localhost:44394",
                                             "http://localhost:3000")
                                .WithMethods("POST", "GET");
                        });
                });
            }

            services.AddDbContext<UserManagementDbContext>(options => options
                .UseSqlServer(Configuration.GetConnectionString("UserManagementAppConnectionString")));

            services
                .AddControllers(mvcOptions => mvcOptions.EnableEndpointRouting = false)
                .AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddOData();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "User Management Api", Version = "v1" });
            });

            AddModules(services);
            AddMvcCoreOptions(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseCors(DevelopmentPolicyCorsPolicy);
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseMvc(routeBuilder =>
            {
                routeBuilder.Select().Expand().Filter().OrderBy().MaxTop(null).Count();
                routeBuilder.MapODataServiceRoute("odata", "odata", GetEdmModel());
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "User Management Api V1");
            });
        }

        private void AddModules(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
        }

        private void AddMvcCoreOptions(IServiceCollection services)
        {
            services.AddMvcCore(options =>
            {
                #region OData filters 
                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                #endregion
                options.Filters.Add(typeof(ValidateModelFilter));
            });
        }

        private IEdmModel GetEdmModel()
        {
            var odataBuilder = new ODataConventionModelBuilder();
            odataBuilder.EntitySet<User>("Users").EntityType.Filter(nameof(User.PhoneNumbers));
            odataBuilder.EntitySet<Phone>("Phone");

            return odataBuilder.GetEdmModel();
        }
    }
}
