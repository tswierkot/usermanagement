﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Persistance.DbContext;
using Persistance.Entities;

namespace UserManagementApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhonesController : ControllerBase
    {
        private readonly UserManagementDbContext _userManagementDbContext;

        public PhonesController(UserManagementDbContext userManagementDbContext)
        {
            _userManagementDbContext = userManagementDbContext;
        }

        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<IEnumerable<Phone>>> GetPhones()
        {
            return await _userManagementDbContext.Phones.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Phone>> GetPhone(Guid id)
        {
            var phone = await _userManagementDbContext
                .Phones
                .AsNoTracking()
                .FirstOrDefaultAsync(phone => phone.Id == id);

            if (phone == null)
            {
                return NotFound();
            }

            return phone;
        }

        [HttpPost]
        public async Task<ActionResult<Phone>> PostPhone(Phone phone)
        {
            _userManagementDbContext.Phones.Add(phone);
            await _userManagementDbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPhone), new { id = phone.Id }, phone);
        }

        private bool PhoneExists(Guid id)
        {
            return _userManagementDbContext.Phones.Any(e => e.Id == id);
        }
    }
}
