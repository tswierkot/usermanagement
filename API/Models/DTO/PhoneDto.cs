﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.DTO
{
    public class PhoneDto
    {
        public Guid Id { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public virtual Guid UserId { get; set; }
    }
}
