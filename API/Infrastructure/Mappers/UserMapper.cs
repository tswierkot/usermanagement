﻿using Models.DTO;
using Persistance.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Mappers
{
    public static class UserMapper
    {
        public static UserDto ConvertToDTO(this User user)
        {
            return new UserDto { Id = user.Id, Name = user.Name, Surname = user.Surname, PhoneNumbers = user.PhoneNumbers?.ConvertToDTO() };
        }

        public static IEnumerable<UserDto> ConvertToDTO(this IEnumerable<User> users)
        {
            return users?.Select(user => user.ConvertToDTO());
        }

        public static User ConvertToEntity(this UserDto userDto)
        {
            return new User { Id = userDto.Id, Name = userDto.Name, Surname = userDto.Surname, PhoneNumbers = userDto.PhoneNumbers?.ConvertToEntity().ToList() };
        }
    }
}
