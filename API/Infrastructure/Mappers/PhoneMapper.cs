﻿using Models.DTO;
using Persistance.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Mappers
{
    public static class PhoneMapper
    {
        public static PhoneDto ConvertToDTO(this Phone phone)
        {
            return new PhoneDto { Id = phone.Id, PhoneNumber = phone.PhoneNumber, UserId = phone.UserId};
        }

        public static IEnumerable<PhoneDto> ConvertToDTO(this IEnumerable<Phone> phones)
        {
            return phones?.Select(phone => phone.ConvertToDTO());
        }

        public static Phone ConvertToEntity(this PhoneDto phoneDto)
        {
            return new Phone { Id = phoneDto.Id, PhoneNumber = phoneDto.PhoneNumber, UserId = phoneDto.UserId };
        }

        public static IEnumerable<Phone> ConvertToEntity(this IEnumerable<PhoneDto> phonesDto)
        {
            return phonesDto?.Select(phoneDto => phoneDto.ConvertToEntity());
        }
    }
}
